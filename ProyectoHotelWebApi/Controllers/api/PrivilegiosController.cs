﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ProyectoApiHotel.Models;
using ProyectoHotelWebApi.Models;

namespace ProyectoHotelWebApi.Controllers.api
{
    public class PrivilegiosController : Controller
    {
        private ApiContexto db = new ApiContexto();

        // GET: Privilegios
        public ActionResult Index()
        {
            return View(db.Privilegio.ToList());
        }

        // GET: Privilegios/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Privilegios privilegios = db.Privilegio.Find(id);
            if (privilegios == null)
            {
                return HttpNotFound();
            }
            return View(privilegios);
        }

        // GET: Privilegios/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Privilegios/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "IdPrivilegio,Privilegio")] Privilegios privilegios)
        {
            if (ModelState.IsValid)
            {
                db.Privilegio.Add(privilegios);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(privilegios);
        }

        // GET: Privilegios/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Privilegios privilegios = db.Privilegio.Find(id);
            if (privilegios == null)
            {
                return HttpNotFound();
            }
            return View(privilegios);
        }

        // POST: Privilegios/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "IdPrivilegio,Privilegio")] Privilegios privilegios)
        {
            if (ModelState.IsValid)
            {
                db.Entry(privilegios).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(privilegios);
        }

        // GET: Privilegios/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Privilegios privilegios = db.Privilegio.Find(id);
            if (privilegios == null)
            {
                return HttpNotFound();
            }
            return View(privilegios);
        }

        // POST: Privilegios/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Privilegios privilegios = db.Privilegio.Find(id);
            db.Privilegio.Remove(privilegios);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
