﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using ProyectoApiHotel.Models;
using ProyectoHotelWebApi.Models;

namespace ProyectoHotelWebApi.Controllers.api
{
    public class TipoHabitacionesController : ApiController
    {
        private ApiContexto db = new ApiContexto();

        // GET: api/TipoHabitaciones
        public IQueryable<TipoHabitaciones> GetTipoHabitacion()
        {
            return db.TipoHabitacion;
        }

        // GET: api/TipoHabitaciones/5
        [ResponseType(typeof(TipoHabitaciones))]
        public IHttpActionResult GetTipoHabitaciones(int id)
        {
            TipoHabitaciones tipoHabitaciones = db.TipoHabitacion.Find(id);
            if (tipoHabitaciones == null)
            {
                return NotFound();
            }

            return Ok(tipoHabitaciones);
        }

        // PUT: api/TipoHabitaciones/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutTipoHabitaciones(int id, TipoHabitaciones tipoHabitaciones)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != tipoHabitaciones.IdTipoHabitacion)
            {
                return BadRequest();
            }

            db.Entry(tipoHabitaciones).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TipoHabitacionesExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/TipoHabitaciones
        [ResponseType(typeof(TipoHabitaciones))]
        public IHttpActionResult PostTipoHabitaciones(TipoHabitaciones tipoHabitaciones)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.TipoHabitacion.Add(tipoHabitaciones);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = tipoHabitaciones.IdTipoHabitacion }, tipoHabitaciones);
        }

        // DELETE: api/TipoHabitaciones/5
        [ResponseType(typeof(TipoHabitaciones))]
        public IHttpActionResult DeleteTipoHabitaciones(int id)
        {
            TipoHabitaciones tipoHabitaciones = db.TipoHabitacion.Find(id);
            if (tipoHabitaciones == null)
            {
                return NotFound();
            }

            db.TipoHabitacion.Remove(tipoHabitaciones);
            db.SaveChanges();

            return Ok(tipoHabitaciones);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool TipoHabitacionesExists(int id)
        {
            return db.TipoHabitacion.Count(e => e.IdTipoHabitacion == id) > 0;
        }
    }
}