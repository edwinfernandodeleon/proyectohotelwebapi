﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using ProyectoApiHotel.Models;
using ProyectoHotelWebApi.Models;

namespace ProyectoHotelWebApi.Controllers.api
{
    public class HotelesController : ApiController
    {
        private ApiContexto db = new ApiContexto();

        // GET: api/Hoteles
        public IQueryable<Hoteles> GetHotel()
        {
            return db.Hotel;
        }

        // GET: api/Hoteles/5
        [ResponseType(typeof(Hoteles))]
        public IHttpActionResult GetHoteles(int id)
        {
            Hoteles hoteles = db.Hotel.Find(id);
            if (hoteles == null)
            {
                return NotFound();
            }

            return Ok(hoteles);
        }

        // PUT: api/Hoteles/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutHoteles(int id, Hoteles hoteles)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != hoteles.IdHotel)
            {
                return BadRequest();
            }

            db.Entry(hoteles).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!HotelesExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Hoteles
        [ResponseType(typeof(Hoteles))]
        public IHttpActionResult PostHoteles(Hoteles hoteles)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Hotel.Add(hoteles);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = hoteles.IdHotel }, hoteles);
        }

        // DELETE: api/Hoteles/5
        [ResponseType(typeof(Hoteles))]
        public IHttpActionResult DeleteHoteles(int id)
        {
            Hoteles hoteles = db.Hotel.Find(id);
            if (hoteles == null)
            {
                return NotFound();
            }

            db.Hotel.Remove(hoteles);
            db.SaveChanges();

            return Ok(hoteles);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool HotelesExists(int id)
        {
            return db.Hotel.Count(e => e.IdHotel == id) > 0;
        }
    }
}