﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using ProyectoApiHotel.Models;
using ProyectoHotelWebApi.Models;

namespace ProyectoHotelWebApi.Controllers.api
{
    public class HabitacionesController : ApiController
    {
        private ApiContexto db = new ApiContexto();

        // GET: api/Habitaciones
        public IQueryable<Habitaciones> GetHabitacion()
        {
            return db.Habitacion;
        }

        // GET: api/Habitaciones/5
        [ResponseType(typeof(Habitaciones))]
        public async Task<IHttpActionResult> GetHabitaciones(int id)
        {
            Habitaciones habitaciones = await db.Habitacion.FindAsync(id);
            if (habitaciones == null)
            {
                return NotFound();
            }

            return Ok(habitaciones);
        }

        // PUT: api/Habitaciones/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutHabitaciones(int id, Habitaciones habitaciones)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != habitaciones.IdHabitacion)
            {
                return BadRequest();
            }

            db.Entry(habitaciones).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!HabitacionesExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Habitaciones
        [ResponseType(typeof(Habitaciones))]
        public async Task<IHttpActionResult> PostHabitaciones(Habitaciones habitaciones)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Habitacion.Add(habitaciones);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = habitaciones.IdHabitacion }, habitaciones);
        }

        // DELETE: api/Habitaciones/5
        [ResponseType(typeof(Habitaciones))]
        public async Task<IHttpActionResult> DeleteHabitaciones(int id)
        {
            Habitaciones habitaciones = await db.Habitacion.FindAsync(id);
            if (habitaciones == null)
            {
                return NotFound();
            }

            db.Habitacion.Remove(habitaciones);
            await db.SaveChangesAsync();

            return Ok(habitaciones);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool HabitacionesExists(int id)
        {
            return db.Habitacion.Count(e => e.IdHabitacion == id) > 0;
        }
    }
}