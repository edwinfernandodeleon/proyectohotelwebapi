﻿using System.ComponentModel.DataAnnotations;

namespace ProyectoApiHotel.Models
{
    public class Privilegios
    {
        [Key]
        public int IdPrivilegio { get; set; }

        [Required]
        public string Privilegio { get; set; }
    }
}