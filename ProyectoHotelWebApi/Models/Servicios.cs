﻿using System.ComponentModel.DataAnnotations;

namespace ProyectoApiHotel.Models
{
    public class Servicios
    {
        [Key]
        public int IdServicio { get; set; }

        [Required]
        public string Descripcion { get; set; }

        [Required]
        public decimal Precio { get; set; } 

        public int IdReservacion { get; set; }
        public Habitaciones CodigoHabitacion{ get; set; }
    }
}