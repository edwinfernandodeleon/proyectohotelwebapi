﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ProyectoApiHotel.Models
{
    public class Eventos
    {
        [Key]
        public int IdEvento { get; set; }

        [Required]
        public string NombreEvento { get; set; }

        [Required]
        public DateTime FechaEvento { get; set; }

        public int IdHotel  { get; set; }
        public virtual Hoteles Hotel { get; set; }
    }
}