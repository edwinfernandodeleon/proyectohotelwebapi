﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ProyectoApiHotel.Models
{
    public class Facturas
    {
        [Key]
        public int IdFactura { get; set; }

        [Required]
        public DateTime FechaHora { get; set; }
        
        public int IdCliente { get; set; }
        public Clientes Nit { get; set; }

        public int IdReservacion { get; set; }
        public Reservaciones SubTotal { get; set; }

        public int IdServicio { get; set; }
        public Servicios IdReservacionGastos { get; set; }

        [Required]
        public decimal Total { get; set; }
    }
}