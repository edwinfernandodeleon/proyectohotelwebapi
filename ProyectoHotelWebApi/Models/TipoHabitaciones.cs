﻿using System.ComponentModel.DataAnnotations;

namespace ProyectoApiHotel.Models
{
    public class TipoHabitaciones
    {
        [Key]
        public int IdTipoHabitacion { get; set; }

        [Required]
        public string Tipo { get; set; }
    }
}