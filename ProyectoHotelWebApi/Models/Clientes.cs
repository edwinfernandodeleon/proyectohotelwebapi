﻿using System.ComponentModel.DataAnnotations;

namespace ProyectoApiHotel.Models
{
    public class Clientes
    {
        [Key]
        public int IdCliente { get; set; }

        [Required]
        public string Nombre { get; set; }

        [Required]
        public string Nit { get; set; }

        
        public string Direccion { get; set; }

        
        public string Telefono { get; set; }

        
        public string Correo { get; set; }
    }
}