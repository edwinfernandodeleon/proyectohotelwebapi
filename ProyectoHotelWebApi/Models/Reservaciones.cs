﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ProyectoApiHotel.Models
{
    public class Reservaciones
    {
        [Key]
        public int IdReservacion { get; set; }

        [Required]
        public string NombreReservador { get; set; }

        [Required]
        public string Telefono { get; set; }

        [Required]
        public string Correo { get; set; }

        public int IdHabitacion {get;set;}
        public Habitaciones CodigoHabitacion { get; set; }

        [Required]
        public DateTime FechaIngreso { get; set; }

        [Required]
        public DateTime FechaEgreso { get; set; }

        public decimal SubTotal { get; set; }
    }
}