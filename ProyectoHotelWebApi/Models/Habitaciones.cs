﻿using System.ComponentModel.DataAnnotations;

namespace ProyectoApiHotel.Models
{
    public class Habitaciones
    {
        [Key]
        public int IdHabitacion { get; set; }

        [Required]
        public string CodigoHabitacion { get; set; }

        [Required]
        public int NumeroBanos { get; set; }

        [Required]
        public int NumeroCamas { get; set; }

        [Required]
        public decimal PrecioPorDia { get; set; }

        public int IdTipoHabitacion { get; set; }
        public virtual TipoHabitaciones TipoHabitacion { get; set; }
    }
}