﻿using System.ComponentModel.DataAnnotations;

namespace ProyectoApiHotel.Models
{
    public class Usuarios
    {
        [Key]
        public int IdUsuario { get; set; }

        [Required]
        public string Usuario { get; set; }

        [Required]
        public string Contrasena { get; set; }
        
        public int IdPrivilegio { get; set; }
        public virtual Privilegios Privilegio { get; set; }
    }
}