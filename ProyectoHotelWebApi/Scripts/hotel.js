﻿var ModelHotel = function () {
    var instancia = this;
    var hotelUri = 'api/Hoteles/';
    instancia.hoteles = ko.observableArray([]);
    instancia.error = ko.observable();
    instancia.hotelCargado = ko.observable();

    instancia.hotelNuevo = {
        Nombre: ko.observable(),
        Direccion: ko.observable(),
        Telefono: ko.observable(),
        NombreAdministrador: ko.observable(),
    }

    instancia.getAllHoteles = function () {
        ajaxHelper(hotelUri, 'GET')
        .done(function (data) {
            instancia.hoteles(data);
        });
    }

    instancia.agregar = function () {
        var hotel = {
            Nombre: instancia.hotelNuevo.Nombre(),
            Direccion: instancia.hotelNuevo.Direccion(),
            Telefono: instancia.hotelNuevo.Telefono(),
            NombreAdministrador: instancia.hotelNuevo.NombreAdministrador()
        }
        ajaxHelper(hotelUri, 'POST', hotel)
        .done(function (data) {
            instancia.getAllHoteles();
            $("#modalAgregarHotel").modal('hide');
        });
    }

    instancia.editar = function () {
        var hotel = {
            IdHotel: instancia.hotelCargado().IdHotel,
            Nombre: instancia.hotelCargado().Nombre,
            Direccion: instancia.hotelCargado().Direccion,
            Telefono: instancia.hotelCargado().Telefono,
            NombreAdministrador: instancia.hotelCargado().NombreAdministrador,
        }
        var uriEditar = hotelUri + hotel.IdHotel;
        ajaxHelper(uriEditar, 'PUT', hotel)
        .done(function (data) {
            instancia.getAllHoteles();
            $('#modalEditarHotel').modal('hide');
        });
    }

    instancia.eliminar = function (item) {
        var id = item.IdHotel;
        var uri = hotelUri + id;
        ajaxHelper(uri, 'DELETE').done(function () {
            instancia.getAllHoteles();
        });
    }

    instancia.cargar = function (item) {
        instancia.hotelCargado(item);
    }

    function ajaxHelper(uri, method, data) {
        instancia.error('');
        return $.ajax({
            url: uri,
            type: method,
            dataType: 'json',
            contentType: 'application/json',
            data: data ? JSON.stringify(data) : null
        }).fail(function (jqXHR, textStatus, errorThrown) {
            instancia.error(errorThrown);
        });
    }

    instancia.getAllHoteles();
}

$(document).ready(function () {
    var modelHoteles = new ModelHotel();
    ko.applyBindings(modelHoteles);
});