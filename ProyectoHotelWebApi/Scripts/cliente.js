﻿var ModelCliente = function () {
    var instancia = this;
    var clienteUri = 'api/Clientes/';
    instancia.clientes = ko.observableArray([]);
    instancia.error = ko.observable();
    instancia.clienteCargado = ko.observable();

    instancia.clienteNuevo = {
        Nombre: ko.observable(),
        Nit: ko.observable(),
        Direccion: ko.observable(),
        Telefono: ko.observable(),
        Correo: ko.observable(),
    }

    instancia.getAllClientes = function () {
        ajaxHelper(clienteUri, 'GET')
        .done(function (data) {
            instancia.clientes(data);
        });
    }

    instancia.agregar = function () {
        var cliente = {
            Nombre: instancia.clienteNuevo.Nombre(),
            Nit: instancia.clienteNuevo.Nit(),
            Direccion: instancia.clienteNuevo.Direccion(),
            Telefono: instancia.clienteNuevo.Telefono(),
            Correo: instancia.clienteNuevo.Correo()
        }
        ajaxHelper(clienteUri, 'POST', cliente)
        .done(function (data) {
            instancia.getAllClientes();
            $("#modalAgregarCliente").modal('hide');
        });
    }

    instancia.editar = function () {
        var cliente = {
            IdCliente: instancia.clienteCargado().IdCliente,
            Nombre: instancia.clienteCargado().Nombre,
            Nit: instancia.clienteCargado().Nit,
            Direccion: instancia.clienteCargado().Direccion,
            Telefono: instancia.clienteCargado().Telefono,
            Correo: instancia.clienteCargado().Correo,
        }
        var uriEditar = clienteUri + cliente.IdCliente;
        ajaxHelper(uriEditar, 'PUT', cliente)
        .done(function (data) {
            instancia.getAllClientes();
            $('#modalEditarCliente').modal('hide');
        });
    }

    instancia.eliminar = function (item) {
        var id = item.IdCliente;
        var uri = clienteUri + id;
        ajaxHelper(uri, 'DELETE').done(function () {
            instancia.getAllClientes();
        });
    }

    instancia.cargar = function (item) {
        instancia.clienteCargado(item);
    }

    function ajaxHelper(uri, method, data) {
        instancia.error('');
        return $.ajax({
            url: uri,
            type: method,
            dataType: 'json',
            contentType: 'application/json',
            data: data ? JSON.stringify(data) : null
        }).fail(function (jqXHR, textStatus, errorThrown) {
            instancia.error(errorThrown);
        });
    }

    instancia.getAllClientes();
}

$(document).ready(function () {
    var modelCliente = new ModelCliente();
    ko.applyBindings(modelCliente);
});