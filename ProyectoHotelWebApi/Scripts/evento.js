﻿var ModelEvento = function () {
    var instancia = this;
    var eventoUri = 'api/Eventos/';
    var hotelUri = 'api/Hoteles/';
    instancia.eventos = ko.observableArray([]);
    instancia.hoteles = ko.observableArray([]);
    instancia.error = ko.observable();
    instancia.eventoCargado = ko.observable();

    instancia.eventoNuevo = {
        NombreEvento: ko.observable(),
        FechaEvento: ko.observable(),
        Hotel: ko.observable(),
    }

    instancia.getAllEventos = function () {
        ajaxHelper(eventoUri, 'GET')
        .done(function (data) {
            instancia.eventos(data);
        });
    }

    instancia.getAllHoteles = function () {
        ajaxHelper(hotelUri, 'GET')
        .done(function (data) {
            instancia.hoteles(data);
        });
    }

    instancia.agregar = function () {
        var evento = {
            NombreEvento: instancia.eventoNuevo.NombreEvento(),
            FechaEvento: instancia.eventoNuevo.FechaEvento(),
            IdHotel: instancia.eventoNuevo.Hotel().IdHotel
        }
        ajaxHelper(eventoUri, 'POST', evento)
        .done(function (data) {
            instancia.getAllEventos();
            $("#modalAgregarEvento").modal('hide');
        });
    }

    instancia.editar = function () {
        var evento = {
            IdEvento: instancia.eventoCargado().IdEvento,
            NombreEvento: instancia.eventoCargado().NombreEvento,
            FechaEvento: instancia.eventoCargado().FechaEvento,
            IdHotel: instancia.eventoCargado().Hotel.IdHotel
        }
        var uriEditar = eventoUri + evento.IdEvento;
        ajaxHelper(uriEditar, 'PUT', evento)
        .done(function (data) {
            instancia.getAllEventos();
            $("#modalEditarEvento").modal('hide');
        });
    }

    instancia.eliminar = function (item) {
        var id = item.IdEvento;
        var uri = eventoUri + id;
        ajaxHelper(uri, 'DELETE').done(function () {
            instancia.getAllEventos();
        });
    }

    instancia.cargar = function (item) {
        instancia.eventoCargado(item);
    }

    function ajaxHelper(uri, method, data) {
        instancia.error('');
        return $.ajax({
            url: uri,
            type: method,
            dataType: 'json',
            contentType: 'application/json',
            data: data ? JSON.stringify(data) : null
        }).fail(function (jqXHR, textStatus, errorThrown) {
            instancia.error(errorThrown);
        });
    }

    instancia.getAllEventos();
    instancia.getAllHoteles();
}

$(document).ready(function () {
    var modelEventos = new ModelEvento();
    ko.applyBindings(modelEventos);
});