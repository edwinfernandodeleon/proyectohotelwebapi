﻿var ModelHabitacion = function () {
    var instancia = this;
    var habitacionUri = 'api/Habitaciones/';
    var tipoUri = 'api/TipoHabitaciones/';
    instancia.habitaciones = ko.observableArray([]);
    instancia.tipo = ko.observableArray([]);
    instancia.error = ko.observable();
    instancia.habitacionCargada = ko.observable();

    instancia.habitacionNueva = {
        CodigoHabitacion: ko.observable(),
        NumeroBanos: ko.observable(),
        NumeroCamas: ko.observable(),
        PrecioPorDia: ko.observable(),
        TipoHabitacion: ko.observable()
    }

    instancia.getAllHabitaciones = function () {
        ajaxHelper(habitacionUri, 'GET')
        .done(function (data) {
            instancia.habitaciones(data);
        });
    }

    instancia.getAllTipo = function () {
        ajaxHelper(tipoUri, 'GET')
        .done(function (data) {
            instancia.tipo(data);
        });
    }

    instancia.agregar = function () {
        var habitacion = {
            CodigoHabitacion: instancia.habitacionNueva.CodigoHabitacion(),
            NumeroBanos: instancia.habitacionNueva.NumeroBanos(),
            NumeroCamas: instancia.habitacionNueva.NumeroCamas(),
            PrecioPorDia: instancia.habitacionNueva.PrecioPorDia(),
            IdTipoHabitacion: instancia.habitacionNueva.TipoHabitacion().IdTipoHabitacion
        }
        ajaxHelper(habitacionUri, 'POST', habitacion)
        .done(function (data) {
            instancia.getAllHabitaciones();
            $("#modalAgregarHabitacion").modal('hide');
        });
    }

    instancia.editar = function () {
        var habitacion = {
            IdHabitacion: instancia.habitacionCargada().IdHabitacion,
            CodigoHabitacion: instancia.habitacionCargada().CodigoHabitacion,
            NumeroBanos: instancia.habitacionCargada().NumeroBanos,
            NumeroCamas: instancia.habitacionCargada().NumeroCamas,
            PrecioPorDia: instancia.habitacionCargada().PrecioPorDia,
            IdTipoHabitacion: instancia.habitacionCargada().TipoHabitacion.IdTipoHabitacion
        }
        var uriEditar = habitacionUri + habitacion.IdHabitacion;
        ajaxHelper(uriEditar, 'PUT', habitacion)
        .done(function (data) {
            instancia.getAllHabitaciones();
            $("#modalEditarHabitacion").modal('hide');
        });
    }

    instancia.eliminar = function (item) {
        var id = item.IdHabitacion;
        var uri = habitacionUri + id;
        ajaxHelper(uri, 'DELETE').done(function () {
            instancia.getAllHabitaciones();
        });
    }

    instancia.cargar = function (item) {
        instancia.habitacionCargada(item);
    }

    function ajaxHelper(uri, method, data) {
        instancia.error('');
        return $.ajax({
            url: uri,
            type: method,
            dataType: 'json',
            contentType: 'application/json',
            data: data ? JSON.stringify(data) : null
        }).fail(function (jqXHR, textStatus, errorThrown) {
            instancia.error(errorThrown);
        });
    }

    instancia.getAllHabitaciones();
    instancia.getAllTipo();



    //TODO ESTO PARA TIPO DE HABITACIONES
    var tipoHUri = 'api/TipoHabitaciones/';
    instancia.tipoHabitaciones = ko.observableArray([]);
    instancia.error = ko.observable();
    instancia.tipoCargado = ko.observable();

    instancia.tipoNuevo = {
        Tipo: ko.observable()
    }

    instancia.getAllTipo = function () {
        ajaxHelper(tipoHUri, 'GET')
        .done(function (data) {
            limpiar();
            instancia.tipoHabitaciones(data);
        });
    }

    instancia.agregarTipo = function () {
        var tipo = {
            Tipo: instancia.tipoNuevo.Tipo()
            
        }
        ajaxHelper(tipoHUri, 'POST', tipo)
        .done(function (data) {
            instancia.getAllTipo();
        });
    }

    instancia.editarTipo = function () {
        var tipo = {
            IdTipoHabitacion: instancia.tipoCargado().IdTipoHabitacion,
            Tipo: instancia.tipoCargado().Tipo
        }
        var uriEditar = tipoHUri+ tipo.IdTipoHabitacion;
        ajaxHelper(uriEditar, 'PUT', tipo)
        .done(function (data) {
            instancia.getAllTipo();
            $("#modalEditarTipo").modal('hide');
        });
    }

    instancia.eliminarTipo = function (item) {
        var id = item.IdTipoHabitacion;
        var uri = tipoHUri + id;
        ajaxHelper(uri, 'DELETE').done(function () {
            instancia.getAllTipo();
        });
    }

    instancia.cargarTipo = function (item) {
        instancia.tipoCargado(item);
    }

    function ajaxHelper(uri, method, data) {
        instancia.error('');
        return $.ajax({
            url: uri,
            type: method,
            dataType: 'json',
            contentType: 'application/json',
            data: data ? JSON.stringify(data) : null
        }).fail(function (jqXHR, textStatus, errorThrown) {
            instancia.error(errorThrown);
        });
    }

    function limpiar() {
        instancia.tipoNuevo.Tipo(null);
        instancia.tipoCargado(null);
    }

    instancia.getAllTipo();
    //fin de tipo habitaciones
}

$(document).ready(function () {
    var modelHabitaciones= new ModelHabitacion();
    ko.applyBindings(modelHabitaciones);
});