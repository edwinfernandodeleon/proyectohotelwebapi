namespace ProyectoHotelWebApi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class inicio1 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Habitaciones", "IdHotel", "dbo.Hoteles");
            DropIndex("dbo.Habitaciones", new[] { "IdHotel" });
            DropColumn("dbo.Habitaciones", "IdHotel");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Habitaciones", "IdHotel", c => c.Int(nullable: false));
            CreateIndex("dbo.Habitaciones", "IdHotel");
            AddForeignKey("dbo.Habitaciones", "IdHotel", "dbo.Hoteles", "IdHotel", cascadeDelete: true);
        }
    }
}
