namespace ProyectoHotelWebApi.Migrations
{
    using ProyectoApiHotel.Models;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<ProyectoHotelWebApi.Models.ApiContexto>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(ProyectoHotelWebApi.Models.ApiContexto context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //
            context.Privilegio.AddOrUpdate(
                new Privilegios { IdPrivilegio = 1, Privilegio = "Administrador"},
                new Privilegios { IdPrivilegio = 2, Privilegio = "Trabajador" },
                new Privilegios { IdPrivilegio = 3, Privilegio = "Cliente" }
            );
        }
    }
}
