namespace ProyectoHotelWebApi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class inicio3 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Habitaciones", "Tipo_IdTipoHabitacion", "dbo.TipoHabitaciones");
            DropIndex("dbo.Habitaciones", new[] { "Tipo_IdTipoHabitacion" });
            RenameColumn(table: "dbo.Habitaciones", name: "Tipo_IdTipoHabitacion", newName: "IdTipoHabitacion");
            AlterColumn("dbo.Habitaciones", "IdTipoHabitacion", c => c.Int(nullable: false));
            CreateIndex("dbo.Habitaciones", "IdTipoHabitacion");
            AddForeignKey("dbo.Habitaciones", "IdTipoHabitacion", "dbo.TipoHabitaciones", "IdTipoHabitacion", cascadeDelete: true);
            DropColumn("dbo.Habitaciones", "IdTipo");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Habitaciones", "IdTipo", c => c.Int(nullable: false));
            DropForeignKey("dbo.Habitaciones", "IdTipoHabitacion", "dbo.TipoHabitaciones");
            DropIndex("dbo.Habitaciones", new[] { "IdTipoHabitacion" });
            AlterColumn("dbo.Habitaciones", "IdTipoHabitacion", c => c.Int());
            RenameColumn(table: "dbo.Habitaciones", name: "IdTipoHabitacion", newName: "Tipo_IdTipoHabitacion");
            CreateIndex("dbo.Habitaciones", "Tipo_IdTipoHabitacion");
            AddForeignKey("dbo.Habitaciones", "Tipo_IdTipoHabitacion", "dbo.TipoHabitaciones", "IdTipoHabitacion");
        }
    }
}
