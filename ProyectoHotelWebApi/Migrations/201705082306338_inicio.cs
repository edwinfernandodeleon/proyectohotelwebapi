namespace ProyectoHotelWebApi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class inicio : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Clientes",
                c => new
                    {
                        IdCliente = c.Int(nullable: false, identity: true),
                        Nombre = c.String(nullable: false),
                        Nit = c.String(nullable: false),
                        Direccion = c.String(nullable: false),
                        Telefono = c.String(nullable: false),
                        Correo = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.IdCliente);
            
            CreateTable(
                "dbo.Eventos",
                c => new
                    {
                        IdEvento = c.Int(nullable: false, identity: true),
                        NombreEvento = c.String(nullable: false),
                        FechaEvento = c.DateTime(nullable: false),
                        IdHotel = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.IdEvento)
                .ForeignKey("dbo.Hoteles", t => t.IdHotel, cascadeDelete: true)
                .Index(t => t.IdHotel);
            
            CreateTable(
                "dbo.Hoteles",
                c => new
                    {
                        IdHotel = c.Int(nullable: false, identity: true),
                        Nombre = c.String(nullable: false),
                        Direccion = c.String(nullable: false),
                        Telefono = c.String(nullable: false),
                        NombreAdministrador = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.IdHotel);
            
            CreateTable(
                "dbo.Facturas",
                c => new
                    {
                        IdFactura = c.Int(nullable: false, identity: true),
                        FechaHora = c.DateTime(nullable: false),
                        IdCliente = c.Int(nullable: false),
                        IdReservacion = c.Int(nullable: false),
                        IdServicio = c.Int(nullable: false),
                        Total = c.Decimal(nullable: false, precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.IdFactura)
                .ForeignKey("dbo.Servicios", t => t.IdServicio, cascadeDelete: true)
                .ForeignKey("dbo.Clientes", t => t.IdCliente, cascadeDelete: true)
                .ForeignKey("dbo.Reservaciones", t => t.IdReservacion, cascadeDelete: true)
                .Index(t => t.IdCliente)
                .Index(t => t.IdReservacion)
                .Index(t => t.IdServicio);
            
            CreateTable(
                "dbo.Servicios",
                c => new
                    {
                        IdServicio = c.Int(nullable: false, identity: true),
                        Descripcion = c.String(nullable: false),
                        Precio = c.Decimal(nullable: false, precision: 18, scale: 2),
                        IdReservacion = c.Int(nullable: false),
                        CodigoHabitacion_IdHabitacion = c.Int(),
                    })
                .PrimaryKey(t => t.IdServicio)
                .ForeignKey("dbo.Habitaciones", t => t.CodigoHabitacion_IdHabitacion)
                .Index(t => t.CodigoHabitacion_IdHabitacion);
            
            CreateTable(
                "dbo.Habitaciones",
                c => new
                    {
                        IdHabitacion = c.Int(nullable: false, identity: true),
                        CodigoHabitacion = c.String(nullable: false),
                        NumeroBanos = c.Int(nullable: false),
                        NumeroCamas = c.Int(nullable: false),
                        PrecioPorDia = c.Decimal(nullable: false, precision: 18, scale: 2),
                        IdTipo = c.Int(nullable: false),
                        IdHotel = c.Int(nullable: false),
                        Tipo_IdTipoHabitacion = c.Int(),
                    })
                .PrimaryKey(t => t.IdHabitacion)
                .ForeignKey("dbo.Hoteles", t => t.IdHotel, cascadeDelete: true)
                .ForeignKey("dbo.TipoHabitaciones", t => t.Tipo_IdTipoHabitacion)
                .Index(t => t.IdHotel)
                .Index(t => t.Tipo_IdTipoHabitacion);
            
            CreateTable(
                "dbo.TipoHabitaciones",
                c => new
                    {
                        IdTipoHabitacion = c.Int(nullable: false, identity: true),
                        Tipo = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.IdTipoHabitacion);
            
            CreateTable(
                "dbo.Reservaciones",
                c => new
                    {
                        IdReservacion = c.Int(nullable: false, identity: true),
                        NombreReservador = c.String(nullable: false),
                        Telefono = c.String(nullable: false),
                        Correo = c.String(nullable: false),
                        IdHabitacion = c.Int(nullable: false),
                        FechaIngreso = c.DateTime(nullable: false),
                        FechaEgreso = c.DateTime(nullable: false),
                        SubTotal = c.Decimal(nullable: false, precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.IdReservacion)
                .ForeignKey("dbo.Habitaciones", t => t.IdHabitacion, cascadeDelete: true)
                .Index(t => t.IdHabitacion);
            
            CreateTable(
                "dbo.Privilegios",
                c => new
                    {
                        IdPrivilegio = c.Int(nullable: false, identity: true),
                        Privilegio = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.IdPrivilegio);
            
            CreateTable(
                "dbo.Usuarios",
                c => new
                    {
                        IdUsuario = c.Int(nullable: false, identity: true),
                        Usuario = c.String(nullable: false),
                        Contrasena = c.String(nullable: false),
                        IdPrivilegio = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.IdUsuario)
                .ForeignKey("dbo.Privilegios", t => t.IdPrivilegio, cascadeDelete: true)
                .Index(t => t.IdPrivilegio);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Usuarios", "IdPrivilegio", "dbo.Privilegios");
            DropForeignKey("dbo.Facturas", "IdReservacion", "dbo.Reservaciones");
            DropForeignKey("dbo.Reservaciones", "IdHabitacion", "dbo.Habitaciones");
            DropForeignKey("dbo.Facturas", "IdCliente", "dbo.Clientes");
            DropForeignKey("dbo.Facturas", "IdServicio", "dbo.Servicios");
            DropForeignKey("dbo.Servicios", "CodigoHabitacion_IdHabitacion", "dbo.Habitaciones");
            DropForeignKey("dbo.Habitaciones", "Tipo_IdTipoHabitacion", "dbo.TipoHabitaciones");
            DropForeignKey("dbo.Habitaciones", "IdHotel", "dbo.Hoteles");
            DropForeignKey("dbo.Eventos", "IdHotel", "dbo.Hoteles");
            DropIndex("dbo.Usuarios", new[] { "IdPrivilegio" });
            DropIndex("dbo.Reservaciones", new[] { "IdHabitacion" });
            DropIndex("dbo.Habitaciones", new[] { "Tipo_IdTipoHabitacion" });
            DropIndex("dbo.Habitaciones", new[] { "IdHotel" });
            DropIndex("dbo.Servicios", new[] { "CodigoHabitacion_IdHabitacion" });
            DropIndex("dbo.Facturas", new[] { "IdServicio" });
            DropIndex("dbo.Facturas", new[] { "IdReservacion" });
            DropIndex("dbo.Facturas", new[] { "IdCliente" });
            DropIndex("dbo.Eventos", new[] { "IdHotel" });
            DropTable("dbo.Usuarios");
            DropTable("dbo.Privilegios");
            DropTable("dbo.Reservaciones");
            DropTable("dbo.TipoHabitaciones");
            DropTable("dbo.Habitaciones");
            DropTable("dbo.Servicios");
            DropTable("dbo.Facturas");
            DropTable("dbo.Hoteles");
            DropTable("dbo.Eventos");
            DropTable("dbo.Clientes");
        }
    }
}
