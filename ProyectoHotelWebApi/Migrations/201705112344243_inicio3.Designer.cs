// <auto-generated />
namespace ProyectoHotelWebApi.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class inicio3 : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(inicio3));
        
        string IMigrationMetadata.Id
        {
            get { return "201705112344243_inicio3"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
