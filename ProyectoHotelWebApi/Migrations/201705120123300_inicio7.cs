namespace ProyectoHotelWebApi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class inicio7 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Clientes", "Direccion", c => c.String());
            AlterColumn("dbo.Clientes", "Telefono", c => c.String());
            AlterColumn("dbo.Clientes", "Correo", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Clientes", "Correo", c => c.String(nullable: false));
            AlterColumn("dbo.Clientes", "Telefono", c => c.String(nullable: false));
            AlterColumn("dbo.Clientes", "Direccion", c => c.String(nullable: false));
        }
    }
}
