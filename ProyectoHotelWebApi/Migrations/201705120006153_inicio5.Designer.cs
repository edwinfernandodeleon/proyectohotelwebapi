// <auto-generated />
namespace ProyectoHotelWebApi.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class inicio5 : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(inicio5));
        
        string IMigrationMetadata.Id
        {
            get { return "201705120006153_inicio5"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
